# Welcome to DSPi

This is a repo for digital signal processing projects with Raspberry Pi (RPi). The instructions below assume a Raspberry Pi Model B+ (canakit.com) and a GrovePi+ Starter kit (seeedstudio.com). There are two modes for interacting with the RPi: standard or headless. Standard mode requires a dedicated HDMI monitor and USB mouse/keyboard. Headless works via ethernet, but still requires a one-time set up with monitor and mouse (no keyboard). Skip the one-time set up if your Rpi has been setup already!

## Guidelines for proper use
1. Do not force anything during assembly, everything should fit together without effort. If it seems like you need apply excessive force, then you are doing something wrong.
2. Do not use any permanent adhesives (super glue, hot glue, etc.) to attach any equipment directly to an object. For sensors, the best way is to fabricate a mount (wood or plastic) to which to fasten the sensor via small screws (each chip has small mounting holes). Then you can use a more permanent method to attach the mount. We have 3D printers available if you'd like to create a custom mount.
3. When creating your own code, be sure to always create new files so that updates don't cause conflicts. That is, don't modify existing files.
3. Use the GrovePi+ parts list (back of the cover) to make sure all parts are accounted for.
4. Treat the equipment with care, so that future classes can make use of it as well!

## Setting up the Rapberry Pi (RPi) in Headless Mode

Headless mode allows use to connect to the RPi via an ethernet connection, rather than a dedicated mouse, keyboard, and HDMI monitor (the standard way). If you have these item, you may skip this section and use the RPi as a normal computer. 

GENERAL NOTE: For any setup, make sure everything else is connected before powering the RPi. Otherwise it tends not to recognize connections.

### One-time RP setup without keyboard (Need monitor and mouse)
If the RPi is new out of the box, you still need a monitor and mouse for a one-time set up of the RPi. If your, RPi has already been configured, you can skip to "Login via computer".

1. Set up the hardware via the CanaKit instructions, make sure SD card is properly inserted
2. Connect RP to monitor via HDMI and mouse via USB
3. Turn on the RP and select option to install Raspbian OS
4. After install select it as default boot
5. One the device is booted, follow the wizard to setup the RPi. Skip Wi-Fi and updates.
6. With setup complete, enable SSH and VNC Server
    1. Preferences >> Raspberry Pi Configuration >> Interfaces 
    2. More info on SSH: https://www.raspberrypi.org/documentation/remote-access/ssh/
    3. More info on VNC: https://www.raspberrypi.org/documentation/remote-access/vnc/README.md
7. Shut down the RPi

### Login via computer (need a straight Ethernet cable)

The first time you are running the RPi you need to find the IP adress.
1. Connect via Ethernet first, THEN boot the RPi. Wait about a minute until the ethernet port has established a connection and has an IP address (Check network settings). The green Ethernet port light on the RPi should be on.
2. SSH into the RPi
    1. If you have a Windows computer, you will need to install Putty on your computer: https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
    2. Open Terminal (Mac) or Putty (Windows), then SSH into the RPi. The host is pi@raspberrypi.local, default pw is “raspberry”. Detailed instructions on how to do this are here: https://www.dexterindustries.com/howto/connecting-raspberry-pi-without-monitor-beginners/. You can skip to the part that says either "Windows" or "Mac".
3. Once SSH login is successful, run `ifconfig` on the RPi command line to get the ethernet IP address. This is the address associated with the `eth0` port.

Once you have the IP address, you can log in via RealVNC Viewer from then on. Note that you must use the same port (Ethernet or USB) otherwise the IP address will change.
1. Run the RealVNC Viewer
    1. Download RealVNC Viewer: https://www.realvnc.com/en/connect/download/viewer/
    2. Open RealVNC and create a new connection using the IP from the previous steps
2. (Optional) Use `raspi-config` to adjust resolution: https://help.realvnc.com/hc/en-us/articles/360002249917-VNC-Connect-and-Raspberry-Pi#operating-vnc-server-at-the-command-line-0-6

## Setting up the GrovePi+
NOTE: Run all commands in the RPi command line from the home/pi directory (should be the default directory when you open the command line terminal).

Check the 'home/pi' folder, if the 'Dexter' directory exists, then skip this step.

First, install the GrovePi libraries and clone the GrovePi repo
```bash
curl -kL dexterindustries.com/update_grovepi | bash
```
See here for more info: https://github.com/DexterInd/GrovePi.

## Getting/updating the DSPi libraries
The basic GrovePi libraries do not include the imu and accel/gyro, among others. These are included with this repo (DSPi). 

If the 'home/pi' folder does not contain a 'dpsi' folder, then clone this repo by running
```bash
git clone https://code.vt.edu/vibes-lab/dspi.git
```
In it, you will find updated GrovePi examples as well as IMU and Accel/Gyro libraries for running faster acquisition on these sensors. 

If the 'dpsi' folder already exists, then you can update it the files by running: 
```bash
cd dspi
git pull
```

Use the GrovePi, imu and accel-gyro example codes to acquire some test data and verify that the GrovePi+ is working. Note that not all input slots are the same. There are analog, digital, and I2C connections. Make sure that the sensor is apropriate for those connections (see the examples). The IMU and accel/gyro work only with I2C. You may refer to the GrovePi+ getting started guide for more info on the examples and wiring diagrams.

## Configuring the baud rate for faster acquisition (optional)

The I2C communication speed is determined by the baud rate. By default this is 100 kbits/sec. To allow for faster communication (i.e. faster readings), we can increase it to 400 kbits/sec. Go to Preferences>>Raspberry Pi Configuration and make sure that I2C is enabled. Open the command line and run
```
sudo nano /boot/config.txt
```
Then, scroll down with the arrows keys until you find the line `dtparam=i2c_arm=on` and modify it to
```
dtparam=i2c_arm=on,i2c_arm_baudrate=400000
```
Save the file (Ctrl X >> Y >> Enter) and reboot the RPi for the changes to take effect.

# Sensing

## Inertial measurements

Inertial measurements include acceleration (via accelerometers), angular velocity (via a gyroscope), and magnetic field (via a magnetometer). There are various sensing chips which contain some combination of these sensors. Generally, there are two ways to capture data with these chips: 1) direct read OR 2) reading the FIFO buffer (recommended). The first simply means pulling the most recent sample using a `for` loop, which is good for bearing estimation or other applications where only the current acceleration sample is important. However, because the RPi is a computer and does not execute for loops with consistent timing, this approach does not guarantee a steady sampling rate. Reading using the FIFO buffer resolves this problem and enables faster sampling, at the price of a little more coding. The FIFO buffer is essentially a queue in the sensors' internal memory, which stores a limited number of samples at a fixed rate. These can be read out in blocks as they get put in. The advantage is that the reads do not need to be consistently timed. The disadvantage is that if the FIFO fills up, then you lose data. The code provided will automatically stop the acquisition if the FIFO is full. NOTE: The faster you set the sampling rate, the more likely that you will get a FIFO overflow error.

The DSPi libraries have been designed to both simplify and standardize the FIFO reading process across the various available chips. The repository also provides detailed examples on how to excecute the FIFO sampling using these libraries. 

### IMU (mpu9250)

The mpu9250 is a high performance 9-axis motion tracking (acceleration, gyration, and magnetic field) module with 16-bit resolution and +/- 2g, 4g, 8g and 16 g selectable full scale ranges. 

To run the mpu9250:
1. Plug in the sensor to one of the I2C ports in the GrovePI (labeled "I2C-1","I2C-2", or "I2C-3").
2. Verify that the the GrovePi is detecting the sensor by running `i2cdetect -y 1`. You should see a table with the addresses `04` (GrovePi) and `68` (default address for the mpu9250).
3. Run one of the example python acquisition scripts below. <br>

[Direct read (accel and gyro)](https://code.vt.edu/vibes-lab/dspi/-/blob/master/imu-mpu9250/imu_i2c_direct_read.py) (not recommended for DSP applications)<br>
[Continuous FIFO Read (accel only)](https://code.vt.edu/vibes-lab/dspi/-/blob/master/imu-mpu9250/imu_i2c_fifo_accel.py)<br>
[Continuous FIFO Read (accel and gyro)](https://code.vt.edu/vibes-lab/dspi/-/blob/master/imu-mpu9250/imu_i2c_fifo_accel_gyro_ex.py)<br>

The documentation for the mpu9250 can be found at the following links:
1. Datasheet: https://invensense.tdk.com/wp-content/uploads/2015/02/PS-MPU-9250A-01-v1.1.pdf
2. Register map: https://invensense.tdk.com/download-pdf/mpu-9250-register-map/

NOTES:
1. Test various sampling rates to gauge the limits of the FIFO, this is typically around 500 Hz for a 3 axis sensor (e.g. the accelerometer). This would be halved if you acquire from 6 axes (e.g. accel and gyro). The IMU allows sampling a single axis for the gyro, but not for the accel.
2. The antialiasing filters on the IMU are not high quality, so be conscious that you may be getting aliased signals from higher frequencies. Try to sample as fast as possible without saturating the FIFO.
3. Magnetometer reading is currently not functional.

### Accelerometer-gyroscope (lsm6ds3)

The lsm6ds3 is a 3D digital accelerometer and gyroscope chip with 16-bit resolution and +/- 2g, 4g, 8g and 16 g selectable full scale ranges. It is similar in performance to the mpu9250 and due to a larger FIFO buffer, can generally sample faster (up to 833 Hz).

To run the lsm6ds3:
1. Plug in the sensor to one of the I2C ports in the GrovePI (labeled "I2C-1","I2C-2", or "I2C-3").
2. Verify that the the GrovePi is detecting the sensor by running `i2cdetect -y 1`. You should see a table with the addresses `04` (GrovePi) and `6a` (default address for the lsmds3).
3. Run one of the example python acquisition scripts below. <br>

[Direct read (accel and gyro)](https://code.vt.edu/vibes-lab/dspi/-/blob/master/accel-gyro-lsm6ds3/accel_gyro_i2c_direct_read_ex.py) (not recommended for DSP applications)<br>
[Continuous FIFO Read (accel only)](https://code.vt.edu/vibes-lab/dspi/-/blob/master/accel-gyro-lsm6ds3/lsm_accel_only_i2c_fifo_ex.py)<br>
[Continuous FIFO Read (accel and gyro)](https://code.vt.edu/vibes-lab/dspi/-/blob/master/accel-gyro-lsm6ds3/lsm_accel_gyro_i2c_fifo_ex.py)<br>

The documentation for the lsm6ds3 can be found via the following links:
1. Datasheet and register map: https://www.digikey.com/htmldatasheets/production/1745170/0/0/1/lsm6ds3-datasheet.html?utm_adgroup=xGeneral&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_Product&utm_term=&utm_content=xGeneral&gclid=Cj0KCQjwhY-aBhCUARIsALNIC06s5uaMe4k3F8EKwzFOMTKRW-ih-zcLXPLrxd-XZii1h4by5ROG8lUaApBREALw_wcB

NOTES:
1. Test various sampling rates to gauge the limits of the FIFO, this is typically around 833 Hz for a 3 axis sensor (e.g. the accelerometer). This would be halved if you acquire from 6 axes (e.g. accel and gyro).
2. The antialiasing filters on the LSM are not high quality, so be conscious that you may be getting aliased signals from higher frequencies. Try to sample as fast as possible without saturating the FIFO.

### High-sensitivity digital accelerometer (adxl-357)

The adxl357 is a high-sensitivity digital accelerometer with 20 bit resolution and +/- 10g, 20g, and 40 g selectable full scale ranges. It is best suited to low frequency applications as its small FIFO buffer allows only up to about 250 Hz sampling with the RPi (although the sensor itself can sample up to 4000 Hz) .

To run the adxl357:
1. Plug in the sensor to one of the I2C ports in the GrovePI (labeled "I2C-1","I2C-2", or "I2C-3").
2. Verify that the the GrovePi is detecting the sensor by running `i2cdetect -y 1`. You should see a table with the addresses `04` (GrovePi) and `1D` (default address for the adxl357).
3. Run one of the example python acquisition scripts below. <br>
[Continuous FIFO Read](https://code.vt.edu/vibes-lab/dspi/-/blob/master/accel-adxl357/adxl_i2c_fifo_ex.py)<br>

1. Datasheet: https://www.analog.com/media/en/technical-documentation/data-sheets/adxl356-357.pdf

### Structural Dynamics Kit

This is a custom design kit for structural dynamics experiments on a model building. It is designed to collect data from two IMUs simultaneously: an mpu9250 to measure roof acceleration and an lsm6ds3 to measure base acceleration.

To run the experiment kit:
1. Plug in both sensors to two of the I2C ports in the GrovePI (labeled "I2C-1","I2C-2", or "I2C-3").
2. Verify that the the GrovePi is detecting the sensors by running `i2cdetect -y 1` in the command line. You should see a table with the addresses `04` (GrovePi), `6a` (default address for the lsmds3), and `68` (default address for the mpu9250).
3. Access the script via the file explorer. Go to `/home/pi/dspi/structural-dynamics-kit/` 
4. Open the acquisition script below (double click), which open a python interface (usually Thonny). Run it by clicking the green run button. <br>
[mainBuildingTest.py](https://code.vt.edu/vibes-lab/dspi/-/blob/master/structural-dynamics-kit/mainBuildingTest.py?ref_type=heads)<br>
5. You can change the amount of data you collect by modifying T and fs parameters in the "Acquisition Settings" section of the code. fs = 100 is recommended.
6. Data is automatically saved in the path indicated by the `save_name` variable. By default this is the Test_Files folder on the Desktop. Change the variable name to save a different file. You can export data via USB or email if you have an internet connection.


## RPi camera

In depth instructions how to set up and run the RPi camera can be found here: https://projects.raspberrypi.org/en/projects/getting-started-with-picamera/2

# Signal processing

Signal processing in python can be excecuted using Scypi, particularly the [Scipy.signal](https://docs.scipy.org/doc/scipy/reference/signal.html) and [Scipy.fft](https://docs.scipy.org/doc/scipy/reference/tutorial/fft.html) libraries. Their format is very similar to Matlab, and so they should be easy to implement.

Note that in order to obtain the full functionality of these libraries you will need to updgrade the default Scipy libraries that come with the RPi. Scipy is built on top on Numpy, so it's best to upgrade Numpy altogether, and this includes the Scipy udgrade.

```
sudo apt update
sudo apt remove python3-numpy
sudo apt install libatlas3-base
sudo pip3 install numpy
```
