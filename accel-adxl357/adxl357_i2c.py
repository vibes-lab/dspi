# this is to be saved in the local folder under the name "adxl375_i2c.py"
# it will be used as the I2C controller and function harbor for the project 
# refer to datasheet and register map for full explanation

import smbus,time
import sys
import numpy as np

# MPU6050 Registers
ADXL357_ADDR = 0x1D
STATUS       = 0x04
FIFO_ENTRIES = 0x05
TEMP1        = 0x07
XDATA1       = 0x0A
YDATA1       = 0x0D
ZDATA1       = 0x10
FIFO_DATA    = 0x11
RESET        = 0x2F
FIFO_SAMPLES = 0x29
RANGE        = 0x2C
POWER_CTL    = 0x2D
FILTER       = 0x28

odr = np.array([4000,2000,1000,500,250,125,62.5,31.25,15.625,7.813,3.906]) #available data rates (hz)
odr_config = [0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A]

accel_config_sel = [0b00000,0b01000,0b10000,0b11000] # byte registers
accel_config_vals = [2.0,4.0,8.0,16.0] # g (g = 9.81 m/s^2)
__SENSOR__ = "none"

def ADXL357_start():
    
    #bus.write_byte_data(ADXL357_ADDR,RESET,0x52) # software reset, may cause faulty operation
    bus.write_byte_data(ADXL357_ADDR,RANGE,0b10000011)
    bus.write_byte_data(ADXL357_ADDR,POWER_CTL,0b0001) #standby mode 
    time.sleep(1)
        
def setLPF_accel(cutoff):
    print('For this device, the anti-alias low pass filter is automatically set to 1/4 of the sampling rate with setRate()')

def getLPF_accel():
    #Get the low pass filter cutoff frequency
    rate = getRate() 
    return rate/4.0

def setRange_accel(rang):
    #Set the accelrometer full scale output range
    res = bus.read_byte_data(ADXL357_ADDR, RANGE) & 0b11111100
    if rang == '10g':
        bus.write_byte_data(ADXL357_ADDR, RANGE, res | 0b01)
    elif rang == '20g':
        bus.write_byte_data(ADXL357_ADDR, RANGE, res | 0b10)
    elif rang == '40g':
        bus.write_byte_data(ADXL357_ADDR, RANGE, res | 0b11)
    else:
        print("Range options are '10g', '20g', or '40g'")
        
    time.sleep(.5)

def getRange_accel():
        # Get the full scale range  of accelerometer in +/- g
        if (bus.read_byte_data(ADXL357_ADDR, RANGE) & 0x03) == 0b01:
            return 10.0
        elif (bus.read_byte_data(ADXL357_ADDR, RANGE) & 0x03) == 0b10:
            return 20.0
        elif (bus.read_byte_data(ADXL357_ADDR, RANGE) & 0x03) == 0b11:
            return 40.0

def setRate(rate):
    # sets sampling rate
    hpf = bus.read_byte_data(ADXL357_ADDR, FILTER) & 0b01110000
    
    Imax = np.argmax(odr)
    if rate > odr[Imax]: 
        bus.write_byte_data(ADXL357_ADDR, FILTER, odr_config[Imax] | hpf) #set accel rate (odr)
        print('Requested accel sampling rate is too high, set to {0:2.2f} Hz'.format(odr[Imax]))
    else:
        gt = odr >= rate
        rate_set = np.min(odr[gt])
        I = np.argmax(odr==rate_set)
        bus.write_byte_data(ADXL357_ADDR, FILTER, odr_config[I] | hpf) #set accel rate (odr)

def getRate():
    # read sample rate in hertz, assume accel rate is the same for all (gyro and fifo)
    odri = bus.read_byte_data(ADXL357_ADDR, FILTER) & 0b1111
    
    return odr[odri]

def enable_FIFO(sensor):
    #enable saving sensor data to FIFO buffer (allows faster and more precise sampling)
    global __SENSOR__
    __SENSOR__ = sensor
    
    if (sensor == "accel"):   
        bus.write_byte_data(ADXL357_ADDR,POWER_CTL,0b0000) #measurement mode
        time.sleep(0.1)
    elif (sensor == "none"):
        bus.write_byte_data(ADXL357_ADDR,POWER_CTL,0b0001) #standby mode
        time.sleep(0.1)
        
def rst_FIFO():
    #clear the FIFO, count goes to 0
    bus.write_byte_data(ADXL357_ADDR,POWER_CTL,0b0001) #standby mode
    bus.write_byte_data(ADXL357_ADDR,POWER_CTL,0b0000) #measurement mode
    FIFO_overflow() #call the overflow register to clear it

def count_FIFO():
    # read number of fifo entries
    return bus.read_byte_data(ADXL357_ADDR, FIFO_ENTRIES)

def FIFO_overflow():
    #returns true if the fifo has overflowed
    
    # read the interrupt status register
    int_stat = bus.read_byte_data(ADXL357_ADDR, STATUS)
    # extract bit 4 (overflow status)
    oflow = int_stat & 0b0100
    data_rdy = int_stat & 0b01 #data is ready to read
    
    return oflow, data_rdy

def read_continuous_FIFO(N):
    data = [] #empty data vector
    sensor_num = 3
    
    i=0 #number of time samples
    print("Starting Continuous Acquisition...")
    while i < N:
        #This loop continuously reads the FIFO to check for new samples, it will quit if the FIFO overflows,
        #b/c that means it lost data.
        #time.sleep(.01)
        cnti = count_FIFO() #check number of bytes in the FIFO buffer
        [oflowi,data_rdy] = FIFO_overflow() #check if it overflowed since last read
        #print(cnti)
        #print(oflowi)
        #print(data_rdy)
        if oflowi == False:
            if not(cnti == 0) and data_rdy == True:
                #if not empty
                block = read_block_FIFO()
                #print(block)
                data.extend(block)
                i += len(block)/sensor_num/3 #for each sensor FIFO stores three bytes of data per sample
                #print(i)
            #print('-------')
        else:
            i = N+1
            print("FIFO Overflowed: stopping loop. Try reducing sample rate")
    print("Acquisition Complete.")
    return data

def read_block_FIFO():
    #reads all bytes in the fifo (up to max of 32)
    count = count_FIFO() #samples in the FIFO
    read_count = min(27,count*3) #bytes to read (can only read up to 32)
    block = bus.read_i2c_block_data(ADXL357_ADDR, FIFO_DATA,read_count)
    
    return block

def read_single_FIFO():
    #reads the first sample from the fifo
    byte3 = bus.read_byte_data(ADXL357_ADDR, FIFO_DATA)
    byte2 = bus.read_byte_data(ADXL357_ADDR, FIFO_DATA)
    byte1 = bus.read_byte_data(ADXL357_ADDR, FIFO_DATA)
    
    return [byte3,byte2,byte1]

def conv_FIFO(data,sensor,N=0):
    #adjust sensitivity and number of expected byte blocks based on sensor setting
    if sensor == 'none': 
        return 0
    
    byte_num = 9
    
    if (N == 0 or N > len(data)//byte_num):
        N = len(data)//byte_num #read up to the full sample
    
    accel_range = getRange_accel()
    
    i = 0
    
    a_x = []
    a_y = []
    a_z = []
    while i < N*byte_num:        
        a_x3 = data[i]
        a_x2 = data[i+1]
        a_x1 = data[i+2]
        a_y3 = data[i+3]
        a_y2 = data[i+4]
        a_y1 = data[i+5]
        a_z3 = data[i+6]
        a_z2 = data[i+7]
        a_z1 = data[i+8]
        
        # combine higha and low for unsigned bit value
        isx = a_x1 & 0x01
        if isx:
            a_xi = ((a_x3 << 12) | (a_x2 << 4) | (a_x1 >> 4))
            a_yi = ((a_y3 << 12) | (a_y2 << 4) | (a_y1 >> 4))
            a_zi = ((a_z3 << 12) | (a_z2 << 4) | (a_z1 >> 4))
        else:
            print('Something is wrong, the x-axis marker is not in the expected place in the FIFO')
    
        # convert to +- value
        if(a_xi > 2**20/2):
            a_xi -= 2**20
        if(a_yi > 2**20/2):
            a_yi -= 2**20
        if(a_zi > 2**20/2):
            a_zi -= 2**20
        
        #convert to acceleration in g and gyro dps
        a_xc = (a_xi/(2.0**19.0))*accel_range
        a_yc = (a_yi/(2.0**19.0))*accel_range
        a_zc = (a_zi/(2.0**19.0))*accel_range
    
        a_x.append(a_xc)
        a_y.append(a_yc)
        a_z.append(a_zc)
    
        i += byte_num
    return a_x,a_y,a_z
    

def read_raw_bits(register):
    # read accel and gyro values
    data1 = bus.read_byte_data(ADXL357_ADDR, register)
    data2 = bus.read_byte_data(ADXL357_ADDR, register-1)
    data3 = bus.read_byte_data(ADXL357_ADDR, register-2) & 0xF0 

    # combine higha and low for unsigned bit value
    value = ((data3 << 12) | (data2 << 4) | (data1 >> 4))
    
    # convert to +- value
    if(value > 2**20/2):
        value -= 2**20
    return value

def accel_read():
    # raw acceleration bits
    acc_x = read_raw_bits(XDATA1)
    acc_y = read_raw_bits(YDATA1)
    acc_z = read_raw_bits(ZDATA1)

    accel_range = getRange_accel()
    
    #convert to acceleration in g and gyro dps
    a_x = (acc_x/(2.0**19.0))*accel_range
    a_y = (acc_y/(2.0**19.0))*accel_range
    a_z = (acc_z/(2.0**19.0))*accel_range

##    temp = ((t_val)/333.87)+21.0 # uncomment and add below in return
    return a_x,a_y,a_z

def temp_read():
    # raw temp bits
    th = bus.read_byte_data(ADXL357_ADDR, TEMP1+1)
    tl = bus.read_byte_data(ADXL357_ADDR, TEMP1) 
     
    t_val = ((th<< 8) | tl)
    # convert to +- value
    if(t_val > 2**12/2):
        t_val -= 2**12
    
    temp = (-(t_val-1885)/9.05)+25.0 # uncomment and add below in return
    return t_val


# start I2C driver
bus = smbus.SMBus(1) # start comm with i2c bus
ADXL357_start() # instantiate gyro/accel

