# The MIT License (MIT)
#
# Copyright (c) 2016 Adafruit Industries
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Version del Codigo
version = 'Version 0.5.0'
# Acelerometro
Accel = '1'

# Se importan los paquetes necesarios
import smbus,time,datetime
import numpy as np
import sys
from subprocess import call
import pygame

# Se definen las letras a utilizar
pygame.font.init()
myfont2 = pygame.font.SysFont('Arial', 20)
myfont3 = pygame.font.SysFont('Arial', 12)

# Sensor Addresses
LSM6DS3_ADDRESS   = 0X6A
# Addresses Related to FIFO Settings
FIFO_CTRL1        = 0X06
FIFO_CTRL2        = 0X07
FIFO_CTRL3        = 0X08
FIFO_CTRL5        = 0X0A
# Address for Connection Check
WHO_AM_I          = 0X0F
# Address for Accelerometer Settings
CTRL1_XL          = 0X10
# Address for Correct Reading of FIFO
CTRL3_C           = 0X12
# Addresses for Checking FIFO
FIFO_STATUS1      = 0X3A
FIFO_STATUS2      = 0X3B
# Addresses for Extracting Data
FIFO_DATA_OUT_L   = 0X3E
FIFO_DATA_OUT_H   = 0X3F

# WHO_AM_I
I_AM              = 0X6A

# ODR
F104_HZ_HP        = 0X40 # Antialiasing 50Hz
F208_HZ_HP        = 0X50 # Antialiasing 100Hz
F416_HZ_HP        = 0X60 # Antialiasing 200Hz
F833_HZ_HP        = 0X70 # Antialiasing 400Hz
# FIFO ODR
FIFO_100_HZ       = 0X20
FIFO_200_HZ       = 0X28
FIFO_400_HZ       = 0X30
FIFO_800_HZ       = 0X38

# RANGE
R2G               = 0X00 # +/- 2g
R4G               = 0X04 # +/- 4g
R8G               = 0X0C # +/- 8g
R16G              = 0X08 # +/- 16g

# FIFO MODE
FIFO_MODE         = 0X01
BYPASS_MODE       = 0X00
FIFO_XL           = 0X01
FIF_FTH           = 0X80
FIFO_FULL         = 0X20
FIFO_EMPTY        = 0X10
BDU               = 0X44

# Se define una nueva clase para el acelerometro con sus funciones
class LSM6DS3(object):
    """Accelerometro Tri-axial LSM9DS3.
    """
    def _init_(self, address = LSM6DS3_ADDRESS):
        """Inicializa el accelerometro LSM6DS3 por interfaz I2C.
        """
        # Verifica que el acelerometro este conectado
        if bus.read_byte_data(address, WHO_AM_I) != I_AM:
            raise RuntimeError('No se econtro el ID esperado. Revise su conexion.')
        else:
            self.address = address

    def set_XL(self, rate = F104_HZ_HP, range = R2G):
        """Define el funcionamiento del accelerometro, comenzando la medicion.
        El valor de rate debe ser uno de los siguientes:
        - F104_HZ_HP  = 104Hz
        - F208_HZ_HP  = 208Hz
        - F416_HZ_HP  = 416Hz
        - F833_HZ_HP  = 833Hz
        Por default la frecuencia es 104Hz.
        El valor de range puede ser:
        - R2G  = +/-2g
        - R4G  = +/-4g
        - R8G  = +/-8g
        - R16G = +/-16g
        Por default el rango es +/- 2g.
        """
        bus.write_byte_data(self.address, CTRL1_XL, rate | range)

    def read_XL(self, ThH = 0X00, ThL = 0X0F):
        """Lee la aceleracion como un tuple de
        valores X, Y, Z en unidades de g.
        """
        num = (ThH << 8) | ThL
        print(num)
        block = bus.read_i2c_block_data(self.address, FIFO_DATA_OUT_L, 2*num)
        print(len(block))
        i = 0
        ax = []
        ay = []
        az = []
        while (i + 6 <= 2*num):
            ax.append(int.from_bytes(block[i:i+2],byteorder='little',signed='True'))
            ay.append(int.from_bytes(block[i+2:i+4],byteorder='little',signed='True'))
            az.append(int.from_bytes(block[i+4:i+6],byteorder='little',signed='True'))
            i += 6
        if (bus.read_byte_data(self.address,CTRL1_XL) & 0x0C) == R2G:
            return (2./32768) * np.array([ax,ay,az]).T
        elif (bus.read_byte_data(self.address,CTRL1_XL) & 0x0C) == R4G:
            return (4./32768) * np.array([ax,ay,az]).T
        elif (bus.read_byte_data(self.address,CTRL1_XL) & 0x0C) == R8G:
            return (8./32768) * np.array([ax,ay,az]).T
        elif (bus.read_byte_data(self.address,CTRL1_XL) & 0x0C) == R16G:
            return (16./32768) * np.array([ax,ay,az]).T

    def get_data_rate(self):
        """Obtiene la frecuencia de muestreo del acelerometro.
        """
        if (bus.read_byte_data(self.address, CTRL1_XL) & 0xF0) == F104_HZ_HP:
            return 'Frecuencia de Muestreo: 104Hz'
        elif (bus.read_byte_data(self.address, CTRL1_XL) & 0xF0) == F208_HZ_HP:
            return 'Frecuencia de Muestreo: 208Hz' 
        elif (bus.read_byte_data(self.address, CTRL1_XL) & 0xF0) == F416_HZ_HP:
            return 'Frecuencia de Muestreo: 416Hz' 
        elif (bus.read_byte_data(self.address, CTRL1_XL) & 0xF0) == F833_HZ_HP:
            return 'Frecuencia de Muestreo: 833Hz'

    def set_FIFO(self, mode = FIFO_MODE):
        """Define el modo de operacion del acelerometro. Las opciones son:
        - Bypass
        - FIFO
        Por default se define el modo FIFO a 100Hz.
        """
        if mode == FIFO_MODE:
            bus.write_byte_data(self.address, CTRL3_C, BDU)
            bus.write_byte_data(self.address, FIFO_CTRL3, FIFO_XL)
            self.set_th_FIFO()
            if (bus.read_byte_data(self.address, CTRL1_XL) & 0xF0) == F104_HZ_HP:
                bus.write_byte_data(self.address, FIFO_CTRL5, FIFO_100_HZ | FIFO_MODE)
            elif (bus.read_byte_data(self.address, CTRL1_XL) & 0xF0) == F208_HZ_HP:
                bus.write_byte_data(self.address, FIFO_CTRL5, FIFO_200_HZ | FIFO_MODE)
            elif (bus.read_byte_data(self.address, CTRL1_XL) & 0xF0) == F416_HZ_HP:
                bus.write_byte_data(self.address, FIFO_CTRL5, FIFO_400_HZ | FIFO_MODE)
            elif (bus.read_byte_data(self.address, CTRL1_XL) & 0xF0) == F833_HZ_HP:
                bus.write_byte_data(self.address, FIFO_CTRL5, FIFO_800_HZ | FIFO_MODE)
                
        elif mode == BYPASS_MODE:
            bus.write_byte_data(self.address, FIFO_CTRL5, BYPASS_MODE)         

    def get_FIFO(self):
        """Obtiene el modo de operacion del acelerometro.
        """   
        if (bus.read_byte_data(self.address, FIFO_CTRL5) & FIFO_MODE) == FIFO_MODE:
            return 'Operacion en modo FIFO'
        elif (bus.read_byte_data(self.address, FIFO_CTRL5) & FIFO_MODE) == BYPASS_MODE:
            return 'Operacion en modo Bypass'
            
    def read_FIFO(self):
        """Obtiene cuantos datos estan almacenados en el FIFO.
        """
        raw1 = self._device.readU8(FIFO_STATUS1)
        raw2 = self._device.readU8(FIFO_STATUS2)
        return ((raw2 << 8) | raw1)
        
    def set_th_FIFO(self, ThH = 0X00, ThL = 0X1E):
        """Setea el Threshold del FIFO
        """
        bus.write_byte_data(self.address, FIFO_CTRL1, ThL)
        bus.write_byte_data(self.address, FIFO_CTRL2, ThH & 0X0F)

    def thres_FIFO(self):
        """Obtiene si se alcanzo el Threshold o no.
        """
        return bus.read_byte_data(self.address, FIFO_STATUS2) & 0X80
    
    def stat_FIFO(self):
        """Obtiene si el FIFO fue saturado o no.
        """
        return bus.read_byte_data(self.address, FIFO_STATUS2) & 0x20
    
    def get_range(self):
        """Obtiene el rango del acelerometro.
        """     
        if (bus.read_byte_data(self.address, CTRL1_XL) & 0x0C) == R2G:
            return 'Rango: +/- 2g'
        elif (bus.read_byte_data(self.address, CTRL1_XL) & 0x0C) == R4G:
            return 'Rango: +/- 4g'
        elif (bus.read_byte_data(self.address, CTRL1_XL) & 0x0C) == R8G:
            return 'Rango: +/- 8g' 
        elif (bus.read_byte_data(self.address, CTRL1_XL) & 0x0C) == R16G:
            return 'Rango: +/- 16g'
        
# Diccionario que define ubicaciones del texto en el log
N = {}
    
# Se define el posicionamiento del texto en el log
P = {}
for l in range (0, 10):
    P[l] = (100, 490 + l * 20)

# Valor que indica la posicion del texto
w = 0

# Valor que indica el numero de medicion
x = 1

# Valor para definir la frecuencia de muestreo
frec = 0

# Valor para definir el rango
rang = 4

# Vector para eliminar la gravedad en la medicion
Bias = np.array([0, 0, 0])

# Directorio de los archivos
out_name = '/media/pi/5ADA-3521/Mediciones/'

# Funcion para escribir dentro de la interfaz   
def escribir(texto):
    """Escribe en el log de la interfaz grafica.
    """
    if w < 10:
        N[w] = texto
    else:
        for l in range (0, 9):
            N[l] = N[l + 1]
        N[9] = texto
        
# Funcion para reescribir el texto ante cambio en la interfaz
def Actdis():
    """Actualiza el texto en la interfaz grafica.
    """
    if w == 0:
        screen.blit(N[0], P[0])
    elif w < 10:
        for l in range (0, w + 1):
            screen.blit(N[l], P[l])
    else:
        for l in range (0, 10):
            screen.blit(N[l], P[l])

# Funcion que setea la interfaz grafica
def GUI(instrucc):
    """Setea la interfaz grafica.
    """
    screen.fill((0, 0, 0))
    screen.blit(gui, (0, 0))
    screen.blit(label3, (100, 370))
    screen.blit(label4, (100, 410))
    screen.blit(label9, (840, 230))
    screen.blit(instrucc, (465, 370))
    Actdis()
    pygame.display.flip()

# Diccionario para poder cambiar la configuracion desde el menu
Settings = {0 : F104_HZ_HP, 1 : F208_HZ_HP, 2 : F416_HZ_HP, 3 : F833_HZ_HP, 4 : R2G, 5 : R4G, 6: R8G, 7 : R16G}

# Funcion para cambiar la configuracion
def chset():
    """Funcion para cambiar la configuracion.
    """
    A.set_XL(Settings[frec], Settings[rang])

# Se inicia el Acelerometro y se setea perfomance
A=LSM6DS3()
# Setea interfaz I2C para el dispositivo
bus = smbus.SMBus(1)
A._init_()
A.set_XL(F104_HZ_HP, R2G)
A.set_FIFO(BYPASS_MODE)

# Listado de Mensajes
label2 = myfont3.render('Inicializando...', False, (255, 255, 255))
label3 = myfont2.render(A.get_data_rate(), False, (255, 255, 255))
label4 = myfont2.render(A.get_range(), False, (255, 255, 255))
label5 = myfont3.render('Listo', False, (255, 255, 255))
label6 = myfont3.render('Midiendo...', False, (255, 255, 255))
label7 = myfont3.render('Calibrando...', False, (255, 255, 255))
label8 = myfont3.render('Error. Por favor, concluir medicion', False, (255, 255, 255))
label9 = myfont2.render(version, False, (0, 0, 0))
label10 = myfont3.render('En menu', False, (255, 255, 255))
label11 = myfont3.render('En configuracion', False, (255, 255, 255))

# Interfaz Grafica
gui = pygame.image.load('GUI 1024x768.png')
calibrando = pygame.image.load('Calibrando.png')
calibrando = pygame.transform.scale(calibrando, (480, 320))
listo = pygame.image.load('Listo.png')
listo = pygame.transform.scale(listo, (480, 320))
enmenu = pygame.image.load('Menu.png')
enmenu = pygame.transform.scale(enmenu, (480, 320))
midiendo = pygame.image.load('Midiendo.png')
midiendo = pygame.transform.scale(midiendo, (480, 320))
config = pygame.image.load('Config.png')
config = pygame.transform.scale(config, (480, 320))

# Se inicia la interfaz grafica
pygame.init()
pygame.mouse.set_visible(False)
screen = pygame.display.set_mode((0, 0))

# Comienza el funcionamiento con el nombre del LABDIN
N[w] = label2
GUI(calibrando)
w = w + 1

# El dispositivo se ha inicializado
N[w] = label5
GUI(listo)
w = w + 1

# El codigo en si mismo
while True:

    # Lee el teclado
    for event in pygame.event.get(pygame.KEYUP):
      
        # Si se presiona "Arriba" comienza a medir
        if event.key == pygame.K_UP:
            
            # Se borran todos los Inputs
            pygame.event.clear()
            
            # Se senala el arranque
            escribir(label6)
            GUI(midiendo)
            w = w + 1
            
            # Se toma el horario de inicio
            tiempo = datetime.datetime.now()

            # Valor que indica el numero de archivo en una medicion
            z = 0

            # Valor que indica cuantos toma de datos se hicieron en un archivo
            count = 0
                
            # Valor que permite eliminar los primeros 32 datos (inestables)
            remove = 0
            
            # Termino para el funcionamiento del Loop
            Medir = True
            
            # Se abre un archivo de medicion y se escribe el encabezado
            file = open(out_name + 'Acc_' + Accel + '_Med_' + str(x) + '_' + str(z) + '.txt', 'w')
            file.write(str(tiempo) + '. Las columnas responden a x[g], y[g], z[g]. ' + A.get_data_rate() + '\n')
                
            # Se inicia el modo FIFO
            A.set_FIFO()

            # El ciclo de Medicion hasta que se presiona "Abajo"
            while Medir == True:

                # Si el FIFO esta saturado indica error
                if A.stat_FIFO() == 0x20:

                    # Se senala el error si no se indico ya
                    if w < 11:
                        if N[w-1] != label8:
                            escribir(label8)
                            GUI(midiendo)
                            w = w + 1
                    else:
                        if N[9] != label8:
                            escribir(label8)
                            GUI(midiendo)
                            w = w + 1

                    # Cierra el archivo terminado
                    file.close()

                    # Se resetean los parametros
                    A.set_FIFO(BYPASS_MODE)
                    x = x + 1
                    z = 0

                    # Se senala el fin de la medicion
                    escribir(label5)
                    GUI(listo)
                    w = w + 1

                    # Se senala el arranque
                    escribir(label6)
                    GUI(midiendo)
                    w = w + 1
                    
                    # Se toma el horario de inicio
                    tiempo = datetime.datetime.now()

                    # Valor que indica cuantos toma de datos se hicieron en un archivo
                    count = 0
                        
                    # Valor que permite eliminar los primeros 32 datos (inestables)
                    remove = 0
                    
                    # Se abre un archivo de medicion y se escribe el encabezado
                    file = open(out_name + 'Acc_' + Accel + '_Med_' + str(x) + '_' + str(z) + '.txt', 'w')
                    file.write(str(tiempo) + '. Las columnas responden a x[g], y[g], z[g]. ' + A.get_data_rate() + '\n')
                        
                    # Se inicia el modo FIFO
                    A.set_FIFO()   

                # Mira la cantidad datos en el FIFO
                if A.thres_FIFO() == 0X80:
                    file.write(str(np.array([1, -1, 1]) * (A.read_XL() - Bias))+ '\n')
                    count = count + ((0X00 << 8) | 0X0F)

                # Se fija cuantos datos se midieron para crear nuevo archivo
                if count > 21419:

                    # Cierra el archivo finalizado, abre el nuevo
                    file.close()
                    z = z + 1
                    file = open(out_name + 'Acc_' + Accel + '_Med_' + str(x) + '_' + str(z) + '.txt', 'w')
                    file.write(str(tiempo) + '. Las columnas responden a x[g], y[g], z[g]. ' + A.get_data_rate() + '\n')      
                    count = 0                       

                # Lee el teclado
                for event in pygame.event.get(pygame.KEYUP):
                
                    # Si se presiona "Abajo" termina la medicion
                    if event.key == pygame.K_DOWN:
                        Medir = False
                        
                    break

            # Cierra el archivo terminado
            file.close()

            # Se resetean los parametros
            A.set_FIFO(BYPASS_MODE)
            x = x + 1

            # Se senala el fin de la medicion
            escribir(label5)
            GUI(listo)
            w = w + 1

        # Si se presiona "Izquierda" se entra al menu
        elif event.key == pygame.K_LEFT:

            # Se borran todos los Inputs
            pygame.event.clear()
        
            # Termino para el funcionamiento del Loop
            Menu1 = True

            # Se senala que se ingreso al menu
            escribir(label10)
            GUI(enmenu)
            w = w + 1

            # Dentro del Menu
            while Menu1 == True:

                # Se lee el teclado
                for event in pygame.event.get(pygame.KEYUP):
            
                    # Si se presiona "Abajo" se apaga el Raspberry Pi
                    if event.key == pygame.K_DOWN:
                        time.sleep(1)
                        
                        # Se borran todos los Inputs
                        pygame.event.clear()
         
                        # Se apaga el Raspberry Pi
                        call("sudo nohup shutdown -h now", shell = True)
               
                    # Si se presiona "Derecha" se sale del Menu
                    elif event.key == pygame.K_RIGHT:
                        
                        # Se sale del menu
                        Menu1 = False
                        
                        # Se senala la salida del menu
                        escribir(label5)
                        GUI(listo)
                        w = w + 1
               
                    # Si se presiona "Arriba" se termina el programa
                    elif event.key == pygame.K_UP:
                        time.sleep(1)
                        
                        # Se reacomoda la interfaz grafica
                        pygame.display.set_mode()
                        pygame.mouse.set_visible(True)

                        # Se termina el programa
                        sys.exit()
                        
                    # Si se presiona "Izquierda" se entra al segundo Menu   
                    elif event.key == pygame.K_LEFT:

                        # Se borran todos los Inputs
                        pygame.event.clear()
                        
                        # Termino para el funcionamiento del Loop
                        Menu2 = True
                        
                        # Se senala que se entro en la configuracion
                        escribir(label11)
                        GUI(config)
                        w = w + 1
                        
                        # Dentro del Menu
                        while Menu2 == True:
                        
                            # Se lee el teclado
                            for event in pygame.event.get(pygame.KEYUP):
                            
                                # Si se presiona "Arriba" se cambia la frecuencia de muestreo
                                if event.key == pygame.K_UP:
                                    
                                    # Realiza el ciclo
                                    if frec == 3:
                                        frec = 0
                                    else:
                                        frec = frec + 1
                                    chset()
                                    label3 = myfont2.render(A.get_data_rate(), False, (255, 255, 255))
                                    label4 = myfont2.render(A.get_range(), False, (255, 255, 255))
                                    w = w - 1
                                    GUI(config)
                                    w = w + 1
                                    
                                # Si se presiona "Abajo" se reduce la frecuencia de muestreo 
                                elif event.key == pygame.K_DOWN:

                                    # Realiza el ciclo
                                    if rang == 7:
                                        rang = 4
                                    else:
                                        rang = rang + 1
                                    chset()
                                    label3 = myfont2.render(A.get_data_rate(), False, (255, 255, 255))
                                    label4 = myfont2.render(A.get_range(), False, (255, 255, 255))
                                    w = w - 1
                                    GUI(config)
                                    w = w + 1
                                        
                                # Si se presiona "Derecha" se vuelve al primer menu
                                elif event.key == pygame.K_RIGHT:
                                    
                                    # Se sale del segundo menu
                                    Menu2 = False
                                    
                                    # Se senala que se vuelve al menu
                                    escribir(label10)
                                    GUI(enmenu)
                                    w = w + 1
