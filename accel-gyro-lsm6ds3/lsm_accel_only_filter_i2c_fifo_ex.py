# LSM6DS3 6-DoF Example Acquistion

## NOTE: This requires an upgrade of the default RPi scipy library 

from lsm6ds3_i2c import *
import numpy as np
from scipy import fft, signal
import matplotlib.pyplot as plt

time.sleep(0.5) # delay necessary to allow sensor to settle

#==========================================================
# Acquisition settings
T = 3 #acquisition time period (s)
fs = 2000 # "desired" sampling rate
sensor = "accel" #specify the sensor setting, other options include: "gyro" or "accel-gyro"
accel_range = '2g' #accel FSR, 0 = +/- 2g
#==========================================================

enable_FIFO(sensor) #enable the FIFO buffer to read accel X,Y,Z axes

# Set rates and filters
setRate(fs) #set the device sampling rate, NOTE: Sampling rate options are limited, so input may not be actual
# antialias filters are set automatically by the rate selection

# Display the actual values that were set
cutoff_accel = getFilter_accel()
fs_actual = getRate()
print('Sampling Rate = {0:2.2f} Hz'.format(fs_actual))
print('Accel Anti Alias Low Pass Cutoff = {0:2.2f} Hz'.format(cutoff_accel))

# Set and get the full scale range
setRange_accel(accel_range)
accel_range = getRange_accel()
print('Accel Full Scale Range = +/- {0:2.2f} g'.format(accel_range))

# Acquisition
N = T*fs_actual #number of samples to acquire
rst_FIFO() #reset the FIFO to clear any existing data
data = read_continuous_FIFO(N) #collect digital data from the FIFO for a specified number of samples

ax,ay,az = conv_FIFO(data,sensor,N) #convert the digital data to physical units

# Filtering
order = 101
fc = 2 #Hz
h = signal.firwin(order, fc,window='blackman',pass_zero='highpass',fs=fs_actual)
[w,H] = signal.freqz(h,1,fs=fs_actual)

# Visualize filter
plt.subplot(211)
plt.stem(h)
plt.xlabel("Samples")
plt.ylabel("Filter Coefficient")
plt.title("Filter Impulse Response")
plt.subplot(212)
plt.plot(w,np.abs(H))
plt.xlabel("Freq - Hz")
plt.ylabel("Gain - dB")
plt.title("Filter Frequency Response")
plt.show()

ax_f0 = signal.lfilter(h,1,ax)
ax_f = signal.lfilter(h,1,signal.detrend(ax))

# DFT
Ax = np.abs(fft(ax))
Ax_f0 = np.abs(fft(ax_f0))
Ax_f = np.abs(fft(ax_f))

f = np.array(range(0,len(ax)))*fs_actual/len(ax) #frequency range

# Plotting
print('Plotting...')
t = np.array(range(0,len(ax)))/fs_actual
plt.subplot(211)
plt.plot(t,ax,t-order/2/fs_actual,ax_f0,t-order/2/fs_actual,ax_f)
plt.xlabel("Time -s")
plt.ylabel("Acceleration - g")
plt.title("Accel - Time Domain")
plt.legend(['No Filter','Highpass only','Detrend + Highpass'])
plt.subplot(212)
plt.plot(f,Ax,f,Ax_f0,f,Ax_f)
plt.xlabel("Frequency - Hz")
plt.ylabel("DFT Amplitude")
plt.legend(['No Filter','Highpass only','Detrend + Highpass'])
plt.title("Accel - Freq Domain")
plt.show()