# LSM6DS3 6-DoF Example Acquistion

from lsm6ds3_i2c import *
import numpy as np
from scipy import fft, signal
import matplotlib.pyplot as plt

time.sleep(0.5) # delay necessary to allow sensor to settle

#==========================================================
# Acquisition settings
T = 3 #acquisition time period (s)
fs = 500 # "desired" sampling rate
sensor = "accel" #specify the sensor setting, other options include: "gyro" or "accel-gyro"
accel_range = '2g' #accel FSR, 0 = +/- 2g
save_name = 'test1' #change this name save a new file
#==========================================================

enable_FIFO(sensor) #enable the FIFO buffer to read accel X,Y,Z axes

# Set rates and filters
setRate(fs) #set the device sampling rate, NOTE: Sampling rate options are limited, so input may not be actual
# antialias filters are set automatically by the rate selection

# Display the actual values that were set
cutoff_accel = getFilter_accel()
fs_actual = getRate()
print('Sampling Rate = {0:2.2f} Hz'.format(fs_actual))
print('Accel Anti Alias Low Pass Cutoff = {0:2.2f} Hz'.format(cutoff_accel))

# Set and get the full scale range
setRange_accel(accel_range)
accel_range = getRange_accel()
print('Accel Full Scale Range = +/- {0:2.2f} g'.format(accel_range))

# Acquisition
N = T*fs_actual #number of samples to acquire
rst_FIFO() #reset the FIFO to clear any existing data
data = read_continuous_FIFO(N) #collect digital data from the FIFO for a specified number of samples

ax,ay,az = conv_FIFO(data,sensor,N) #convert the digital data to physical units

# DFT
Ax = np.abs(fft(signal.detrend(ax)))
Ay = np.abs(fft(signal.detrend(ay)))
Az = np.abs(fft(signal.detrend(az)))

f = np.array(range(0,len(ax)))*fs_actual/len(ax) #frequency range

# Plotting
t = np.array(range(0,len(ax)))/fs_actual
plt.subplot(211)
plt.plot(t,ax,t,ay,t,az)
plt.xlabel("Time -s")
plt.ylabel("Acceleration - g")
plt.title("Accel - Time Domain")
plt.subplot(212)
plt.plot(f,Ax,f,Ay,f,Az)
plt.xlabel("Frequency - Hz")
plt.ylabel("DFT Amplitude")
plt.title("Accel - Freq Domain")
plt.show()

# Save CSV file
saveA = np.column_stack((t,ax,ay,az))
saveFFT = np.column_stack((f,Ax,Ay,Az))
np.savetxt(save_name+'_accel.csv',saveA,delimiter=",",header="time (s),ax (g),ay (g),az (g)")
np.savetxt(save_name+'_fft.csv',saveFFT,delimiter=",",header="Freq. (Hz),fft(ax),fft(ay),fft(az)")