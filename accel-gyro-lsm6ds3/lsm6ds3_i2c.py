# The MIT License (MIT)
#
# Copyright (c) 2016 Adafruit Industries
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Import libraries
import smbus,time,datetime 
import numpy as np
import sys

# Sensor Addresses
LSM6DS3_ADDR      = 0X6A
# Addresses Related to FIFO Settings
FIFO_CTRL1        = 0X06
FIFO_CTRL2        = 0X07
FIFO_CTRL3        = 0X08
FIFO_CTRL5        = 0X0A
# Address for Connection Check
WHO_AM_I          = 0X0F
# Address for Accelerometer Settings
CTRL1_XL          = 0X10
CTRL2_G           = 0x11
# Address for Correct Reading of FIFO
CTRL3_C           = 0X12
# Addresses for Checking FIFO
FIFO_STATUS1      = 0X3A
FIFO_STATUS2      = 0X3B
# Addresses for Extracting Data
FIFO_DATA_OUT_L   = 0X3E
FIFO_DATA_OUT_H   = 0X3F
# Direct read registers
OUTX_L_XL         = 0x28
OUTY_L_XL         = 0x2A
OUTZ_L_XL         = 0x2C

# WHO_AM_I
I_AM              = 0X6A

# ODR
F104_HZ_HP        = 0X40 # Antialiasing 50Hz
F208_HZ_HP        = 0X50 # Antialiasing 100Hz
F416_HZ_HP        = 0X60 # Antialiasing 200Hz
F833_HZ_HP        = 0X70 # Antialiasing 400Hz
# FIFO ODR
FIFO_100_HZ       = 0X20
FIFO_200_HZ       = 0X28
FIFO_400_HZ       = 0X30
FIFO_800_HZ       = 0X38

# RANGE
R2G               = 0X00 # +/- 2g
R4G               = 0X04 # +/- 4g
R8G               = 0X0C # +/- 8g
R16G              = 0X08 # +/- 16g

# FIFO MODE
FIFO_MODE         = 0b0110
BYPASS_MODE       = 0X00
FIFO_XL           = 0X01
FIFO_G            = 0b001000
FIF_FTH           = 0X80
FIFO_FULL         = 0X20
FIFO_EMPTY        = 0X10
BDU               = 0X44

odr = np.array([104,208,416,833]) #available data rates
odr_config = [F104_HZ_HP,F208_HZ_HP,F416_HZ_HP,F833_HZ_HP]
fifo_odr_config = [FIFO_100_HZ,FIFO_200_HZ,FIFO_400_HZ,FIFO_800_HZ]
__SENSOR__ = "none"

def LSM6DS3_start():
    # reset all sensors
    bus.write_byte_data(LSM6DS3_ADDR,CTRL3_C,0x01)
    time.sleep(1)
    
    #disable the fifo
    bus.write_byte_data(LSM6DS3_ADDR,FIFO_CTRL5,0x00) #set to bypass

def setFilter_gyro(cutoff):
    #set the low pass filter to the closest available cutoff frequency (above requested)
    print('For this device, the anti-alias low pass filter is automatically set by setRate()')

def getFilter_gyro():
    #Get the low pass filter cutoff frequency
    rate = getRate() # gyro fchoice_b (must be one for DLPF to activate)
    if rate == 104:
        return 50.0
    elif rate == 208:
        return 100.0
    elif rate == 416:
        return 200.0
    elif rate ==  833:
        return 400.0

def setRange_gyro(rang): 
    rate = bus.read_byte_data(LSM6DS3_ADDR, CTRL2_G) & 0xF0
    if rang == '250dps':
        bus.write_byte_data(LSM6DS3_ADDR, CTRL2_G, rate | 0b0000)
    elif rang == '500dps':
        bus.write_byte_data(LSM6DS3_ADDR, CTRL2_G, rate | 0b0100)
    elif rang == '1000dps':
        bus.write_byte_data(LSM6DS3_ADDR, CTRL2_G, rate | 0b1000)
    elif rang == '2000dps':
        bus.write_byte_data(LSM6DS3_ADDR, CTRL2_G, rate | 0b1100)
    else:
        print("Range options are '250dps', '500dps', '1000dps', or '2000dps'.")

def getRange_gyro():                          
    # Get the full scale range  of accelerometer in +/- g
    if (bus.read_byte_data(LSM6DS3_ADDR, CTRL2_G) & 0x0C) == R2G:
        return 250.0
    elif (bus.read_byte_data(LSM6DS3_ADDR, CTRL2_G) & 0x0C) == R4G:
        return 500.0
    elif (bus.read_byte_data(LSM6DS3_ADDR, CTRL2_G) & 0x0C) == R8G:
        return 1000.0 
    elif (bus.read_byte_data(LSM6DS3_ADDR, CTRL2_G) & 0x0C) == R16G:
        return 2000.0 
    
    return sens
        
def setFilter_accel(cutoff):
    print('For this device, the anti-alias low pass filter is automatically set by setRate()')

def getFilter_accel():
    #Get the low pass filter cutoff frequency
    rate = getRate() # gyro fchoice_b (must be one for DLPF to activate)
    if rate == 104:
        return 50.0
    elif rate == 208:
        return 100.0
    elif rate == 416:
        return 200.0
    elif rate ==  833:
        return 400.0

def setRange_accel(rang):
    #Set the accelrometer full scale output range
    rate = bus.read_byte_data(LSM6DS3_ADDR, CTRL1_XL) & 0xF0
    if rang == '2g':
        bus.write_byte_data(LSM6DS3_ADDR, CTRL1_XL, rate | R2G)
    elif rang == '4g':
        bus.write_byte_data(LSM6DS3_ADDR, CTRL1_XL, rate | R4G)
    elif rang == '8g':
        bus.write_byte_data(LSM6DS3_ADDR, CTRL1_XL, rate | R8G)
    elif rang == '16g':
        bus.write_byte_data(LSM6DS3_ADDR, CTRL1_XL, rate | R16G)
    else:
        print("Range options are '2g', '4g', '8g', or '16g'.")

def getRange_accel():
        # Get the full scale range  of accelerometer in +/- g
        if (bus.read_byte_data(LSM6DS3_ADDR, CTRL1_XL) & 0x0C) == R2G:
            return 2.0
        elif (bus.read_byte_data(LSM6DS3_ADDR, CTRL1_XL) & 0x0C) == R4G:
            return 4.0
        elif (bus.read_byte_data(LSM6DS3_ADDR, CTRL1_XL) & 0x0C) == R8G:
            return 8.0 
        elif (bus.read_byte_data(LSM6DS3_ADDR, CTRL1_XL) & 0x0C) == R16G:
            return 16.0

def setRate(rate):
    # sets sampling rate
    rang_accel = bus.read_byte_data(LSM6DS3_ADDR, CTRL1_XL) & 0x0C
    rang_gyro = bus.read_byte_data(LSM6DS3_ADDR, CTRL2_G) & 0x0C
    fifo_mode = bus.read_byte_data(LSM6DS3_ADDR, FIFO_CTRL5) & 0b0111
    
    Imax = np.argmax(odr)
    if rate > odr[Imax]: 
        bus.write_byte_data(LSM6DS3_ADDR, CTRL1_XL, odr_config[Imax] | rang_accel) #set accel rate (odr)
        bus.write_byte_data(LSM6DS3_ADDR, CTRL2_G, odr_config[Imax] | rang_gyro) #set gyro rate (odr)
        bus.write_byte_data(LSM6DS3_ADDR, FIFO_CTRL5, fifo_odr_config[Imax] | fifo_mode) #set fifo rate
        print('Requested accel sampling rate is too high, set to {0:2.2f} Hz'.format(odr[Imax]))
    else:
        gt = odr >= rate
        rate_set = np.min(odr[gt])
        I = np.argmax(odr==rate_set)
        bus.write_byte_data(LSM6DS3_ADDR, CTRL1_XL, odr_config[I] | rang_accel) #set accel rate (odr)
        bus.write_byte_data(LSM6DS3_ADDR, CTRL2_G, odr_config[I] | rang_gyro) #set gyro rate (odr)
        bus.write_byte_data(LSM6DS3_ADDR, FIFO_CTRL5, fifo_odr_config[I] | fifo_mode) #set fifo rate (odr)
        
def getRate():
    # read sample rate in hertz, assume accel rate is the same for all (gyro and fifo)
    if (bus.read_byte_data(LSM6DS3_ADDR, CTRL1_XL) & 0xF0) == F104_HZ_HP:
        return 104.00
    elif (bus.read_byte_data(LSM6DS3_ADDR, CTRL1_XL) & 0xF0) == F208_HZ_HP:
        return 208.00
    elif (bus.read_byte_data(LSM6DS3_ADDR, CTRL1_XL) & 0xF0) == F416_HZ_HP:
        return 416.00 
    elif (bus.read_byte_data(LSM6DS3_ADDR, CTRL1_XL) & 0xF0) == F833_HZ_HP:
        return 833.00

def rst_FIFO():
    #clear the FIFO, count goes to 0
    fifo_odr = bus.read_byte_data(LSM6DS3_ADDR, FIFO_CTRL5) & 0b01111000
    bus.write_byte_data(LSM6DS3_ADDR,FIFO_CTRL5,fifo_odr|BYPASS_MODE) #set to bypass
    bus.write_byte_data(LSM6DS3_ADDR,FIFO_CTRL5,fifo_odr|FIFO_MODE) #set back to continuous mode
    FIFO_overflow() #call the overflow register to clear it
    
def enable_FIFO(sensor):
    #enable saving sensor data to FIFO buffer (allows faster and more precise sampling)
    global __SENSOR__
    bus.write_byte_data(LSM6DS3_ADDR, CTRL3_C, BDU)
    set_th_FIFO()
    fifo_odr = bus.read_byte_data(LSM6DS3_ADDR, FIFO_CTRL5) & 0b01111000
    bus.write_byte_data(LSM6DS3_ADDR, FIFO_CTRL5, fifo_odr | FIFO_MODE)
    __SENSOR__ = sensor
    
    if (sensor == "accel"):
        bus.write_byte_data(LSM6DS3_ADDR, FIFO_CTRL3, FIFO_XL)
        time.sleep(0.1)
    elif (sensor == "gyro"):
        bus.write_byte_data(LSM6DS3_ADDR, FIFO_CTRL3, FIFO_G)
        time.sleep(0.1)
    elif (sensor == "accel-gyro"):
        bus.write_byte_data(LSM6DS3_ADDR, FIFO_CTRL3, FIFO_G|FIFO_XL)
        time.sleep(0.1)
    elif (sensor == "none"):
        bus.write_byte_data(LSM6DS3_ADDR, FIFO_CTRL3, 0x00)
        time.sleep(0.1)

def count_FIFO():
    # read high and low FIFO values
    low = bus.read_byte_data(LSM6DS3_ADDR,FIFO_STATUS1)
    high = bus.read_byte_data(LSM6DS3_ADDR,FIFO_STATUS2) & 0x0F

    # combine high and low for unsigned bit value
    fifo_count = ((high << 8) | low)
    
    return fifo_count

def set_th_FIFO(ThH = 0X00, ThL = 0X1E):
        #Set the FIFO Threshold
        bus.write_byte_data(LSM6DS3_ADDR, FIFO_CTRL1, ThL)
        bus.write_byte_data(LSM6DS3_ADDR, FIFO_CTRL2, ThH & 0X0F)

def get_th_FIFO(self):
        # Get wether FIFO threshold was exceeded
        return bus.read_byte_data(LSM6DS3_ADDR, FIFO_STATUS2) & 0X80

def FIFO_overflow():
    #returns true if the fifo has overflowed
    
    # read the interrupt status register
    int_stat = bus.read_byte_data(LSM6DS3_ADDR, FIFO_STATUS2)
    # extract bit 4 (overflow status)
    oflow = int_stat & 0b01000000 != 0
    
    return oflow

def read_continuous_FIFO(N):
    data = [] #empty data vector
    if __SENSOR__ == "accel-gyro":
        sensor_num = 6
    elif __SENSOR__ == "none":
        return 0
    else:
        sensor_num = 3
    i=0 #number of time samples
    print("Starting Continuous Acquisition...")
    while i < N:
        #This loop continuously reads the FIFO to check for new samples, it will quit if the FIFO overflows,
        #b/c that means it lost data.
        #time.sleep(.01)
        cnti = count_FIFO() #check number of bytes in the FIFO buffer
        oflowi = FIFO_overflow() #check if it overflowed since last read
        #print(cnti)
        #print(cnti)
        #print(oflowi)
        if oflowi == False:
            if not(cnti == 0):
                #if not empty
                block = read_block_FIFO()
                data.extend(block)
                #data_l.extend(block_l)
                i += len(block)/sensor_num/2 #for each sensor FIFO stores two bytes of data per sample
        else:
            i = N+1
            print("FIFO Overflowed: stopping loop. Try reducing sample rate")
    print("Acquisition Complete.")
    return data

def read_block_FIFO():
    #reads all bytes in the fifo (up to max of 32)
    count = count_FIFO() #samples in the FIFO
    read_count = min(32,count*2) #bytes to read (can only read up to 32)
    block = bus.read_i2c_block_data(LSM6DS3_ADDR, FIFO_DATA_OUT_L,read_count)
    
    return block

def read_single_FIFO():
    #reads the first byte from the fifo
    byte_h = bus.read_byte_data(LSM6DS3_ADDR, FIFO_DATA_OUT_H)
    byte_l = bus.read_byte_data(LSM6DS3_ADDR, FIFO_DATA_OUT_L)
    
    return [byte_l,byte_h]

def conv_FIFO(data,sensor,N=0):
    #adjust sensitivity and number of expected byte blocks based on sensor setting
    if (sensor == "accel-gyro"):
        byte_num = 12
    else:
        byte_num = 6
    
    if (N == 0 or N > len(data)//byte_num):
        N = len(data)//byte_num #read up to the last full sample
    
    accel_range = getRange_accel()
    gyro_range = getRange_gyro()
    
    i = 0
    
    if (byte_num == 6):
        a_x = []
        a_y = []
        a_z = []
        while i < N*byte_num:        
            a_xl = data[i]
            a_xh = data[i+1]
            a_yl = data[i+2]
            a_yh = data[i+3]
            a_zl = data[i+4]
            a_zh = data[i+5]
            
            # combine higha and low for unsigned bit value
            a_xi = ((a_xh << 8) | a_xl)
            a_yi = ((a_yh << 8) | a_yl)
            a_zi = ((a_zh << 8) | a_zl)
        
            # convert to +- value
            if(a_xi > 32768):
                a_xi -= 65536
            if(a_yi > 32768):
                a_yi -= 65536
            if(a_zi > 32768):
                a_zi -= 65536
            
            #convert to acceleration in g and gyro dps
            if (sensor == "accel"):
                a_xc = (a_xi/(2.0**15.0))*accel_range
                a_yc = (a_yi/(2.0**15.0))*accel_range
                a_zc = (a_zi/(2.0**15.0))*accel_range
            elif (sensor == "gyro"):
                a_xc = (a_xi/(2.0**15.0))*gyro_range
                a_yc = (a_yi/(2.0**15.0))*gyro_range
                a_zc = (a_zi/(2.0**15.0))*gyro_range
        
            a_x.append(a_xc)
            a_y.append(a_yc)
            a_z.append(a_zc)
        
            i += byte_num
        return a_x,a_y,a_z
    elif (byte_num == 12):
        a_x = []
        a_y = []
        a_z = []
        w_x = []
        w_y = []
        w_z = []
        while i < N*byte_num:
            w_xl = data[i]
            w_xh = data[i+1]
            w_yl = data[i+2]
            w_yh = data[i+3]
            w_zl = data[i+4]
            w_zh = data[i+5]
            a_xl = data[i+6]
            a_xh = data[i+7]
            a_yl = data[i+8]
            a_yh = data[i+9]
            a_zl = data[i+10]
            a_zh = data[i+11]
            
            
            # combine higha and low for unsigned bit value
            a_xi = ((a_xh << 8) | a_xl)
            a_yi = ((a_yh << 8) | a_yl)
            a_zi = ((a_zh << 8) | a_zl)
            w_xi = ((w_xh << 8) | w_xl)
            w_yi = ((w_yh << 8) | w_yl)
            w_zi = ((w_zh << 8) | w_zl)
        
            # convert to +- value
            if(a_xi > 32768):
                a_xi -= 65536
            if(a_yi > 32768):
                a_yi -= 65536
            if(a_zi > 32768):
                a_zi -= 65536
            if(w_xi > 32768):
                w_xi -= 65536
            if(w_yi > 32768):
                w_yi -= 65536
            if(w_zi > 32768):
                w_zi -= 65536
            
            #convert to acceleration in g and gyro dps
            a_xc = (a_xi/(2.0**15.0))*accel_range
            a_yc = (a_yi/(2.0**15.0))*accel_range
            a_zc = (a_zi/(2.0**15.0))*accel_range
            w_xc = (w_xi/(2.0**15.0))*gyro_range
            w_yc = (w_yi/(2.0**15.0))*gyro_range
            w_zc = (w_zi/(2.0**15.0))*gyro_range
        
            a_x.append(a_xc)
            a_y.append(a_yc)
            a_z.append(a_zc)
            w_x.append(w_xc)
            w_y.append(w_yc)
            w_z.append(w_zc)
        
            i += byte_num
        return a_x,a_y,a_z,w_x,w_y,w_z

def read_raw_bits(register):
    # read accel and gyro values
    low = bus.read_byte_data(LSM6DS3_ADDR, register)
    high = bus.read_byte_data(LSM6DS3_ADDR, register+1)

    # combine higha and low for unsigned bit value
    value = ((high << 8) | low)
    
    # convert to +- value
    if(value > 32768):
        value -= 65536
    return value

def accel_read():
    # raw acceleration bits
    acc_x = read_raw_bits(OUTX_L_XL)
    acc_y = read_raw_bits(OUTY_L_XL)
    acc_z = read_raw_bits(OUTZ_L_XL)
    
    accel_range = getRange_accel()
    
    #convert to acceleration in g and gyro dps
    a_x = (acc_x/(2.0**15.0))*accel_range
    a_y = (acc_y/(2.0**15.0))*accel_range
    a_z = (acc_z/(2.0**15.0))*accel_range
    
    return a_x,a_y,a_z

def gyro_read():
    # raw gyroscope bits
    gyro_x = read_raw_bits(GYRO_XOUT_H)
    gyro_y = read_raw_bits(GYRO_YOUT_H)
    gyro_z = read_raw_bits(GYRO_ZOUT_H)
    
    gyro_range = getRange_gyro()

    w_x = (gyro_x/(2.0**15.0))*gyro_range
    w_y = (gyro_y/(2.0**15.0))*gyro_range
    w_z = (gyro_z/(2.0**15.0))*gyro_range

##    temp = ((t_val)/333.87)+21.0 # uncomment and add below in return
    return w_x,w_y,w_z

def temp_read():
    # raw temp bits
    t_val = read_raw_bits(TEMP_OUT_H) # uncomment to read temp
    
    temp = ((t_val)/333.87)+21.0 # uncomment and add below in return
    return temp

# start I2C driver
bus = smbus.SMBus(1) # start comm with i2c bus
LSM6DS3_start() # instantiate gyro/accel