# LSM6DS3 6-DoF Example Acquistion

from lsm6ds3_i2c import *
import numpy as np
from scipy import fft, signal
import matplotlib.pyplot as plt

time.sleep(0.5) # delay necessary to allow sensor to settle

#==========================================================
# Acquisition settings
T = 3 #acquisition time period (s)
fs = 200 # "desired" sampling rate
sensor = "accel" #specify the sensor setting, other options include: "gyro" or "accel-gyro"
sensor_num = 3
accel_range = '2g' #accel FSR, 0 = +/- 2g
#==========================================================

enable_FIFO(sensor) #enable the FIFO buffer to read accel X,Y,Z axes

# Set rates and filters
#setDLPF_gyro(184) #set the gyro's digital low pass filter cutoff to the nyquist frequency
#setDLPF_accel(fs/2) #set the accels's digital low pass filter cutoff to the nyquist frequency
setRate(fs) #set the IMU sampling rate, NOTE: Sampling rate options are whole fractions of 1000 Hz, so input may not be actual

# Display the actual values that were set
#cutoff_gyro = getDLPF_gyro()
#cutoff_accel = getDLPF_accel()
fs_actual = getRate()
print('Sampling Rate = {0:2.2f} Hz'.format(fs_actual))
#print('Gyro Digital Low Pass Cutoff = {0:2.2f} Hz'.format(cutoff_gyro))
#print('Accel Digital Low Pass Cutoff = {0:2.2f} Hz'.format(cutoff_accel))

# Set and get sensitivity
setRange_accel(accel_range)
accel_range = getRange_accel()
print('Accel Range = +/- {0:2.2f} g'.format(accel_range))


rst_FIFO() #reset the FIFO to clear any existing data

N = T*fs_actual #number of samples to acquire
data = [] #empty data vector
i=0 #number of time samples
print("Starting Acquisition...")
while i < N:
    #This loop continually reads the FIFO to check for new samples, it will quit if the FIFO overflows,
    #b/c that means it lost data.
    #time.sleep(.01)
    cnti = count_FIFO() #check number of bytes in the FIFO buffer
    oflowi = FIFO_overflow() #check if it overflowed since last read
    #print(cnti)
    #print(oflowi)
    if oflowi == False:
        if not(cnti == 0):
            #if not empty
            single = read_single_FIFO()
            data.extend(single)
            i += 1/sensor_num #for each sensor FIFO stores two bytes of data per sample
        else:
        i = N+1
        print("FIFO Overflowed: stopping loop. Try reducing sample rate")
print("Acquisition Complete.")

ax,ay,az = conv_FIFO(data,sensor,N) #convert the raw byte data to physical units

# DFT
Ax = np.abs(fft(signal.detrend(ax)))
Ay = np.abs(fft(signal.detrend(ay)))
Az = np.abs(fft(signal.detrend(az)))

f = np.array(range(0,len(ax)))*fs_actual/len(ax) #frequency range

# Plotting
t = np.array(range(0,len(ax)))/fs_actual
plt.subplot(211)
plt.plot(t,ax,t,ay,t,az)
plt.xlabel("Time -s")
plt.ylabel("Acceleration - g")
plt.title("Accel - Time Domain")
plt.subplot(212)
plt.plot(f,Ax,f,Ay,f,Az)
plt.xlabel("Frequency - Hz")
plt.ylabel("DFT Amplitude")
plt.title("Accel - Freq Domain")
plt.show()