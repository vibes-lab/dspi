# MPU9250 Simple Visualization Code
# In order for this to run, the mpu9250_i2c file needs to 
# be in the local folder

from lsm6ds3_i2c import *
import numpy as np
from scipy import fft, signal
import matplotlib.pyplot as plt

plt.style.use('ggplot') # matplotlib visual style setting

time.sleep(1) # wait for mpu9250 sensor to settle

#==========================================================
# Acquisition settings
T = 5 #acquisition time period (s)
fs = 100 # "desired" sampling rate
accel_range = '2g' #accel sensitivity setting, 0 = +/- 2g
sensor = "accel" #specify the sensor setting, other options include: "gyro" or "accel-gyro"
enable_FIFO(sensor) #enable the FIFO buffer to read accel X,Y,Z axes
#==========================================================

N = T*fs # number of points
dt = 1/fs

# Set and get sensitivity
setRange_accel(accel_range)
accel_range = getRange_accel()
print('Accel Range = +/- {0:2.2f} g'.format(accel_range))

# prepping for visualization
mpu9250_str = ['accel-x','accel-y','accel-z','gyro-x','gyro-y','gyro-z']
#AK8963_str = ['mag-x','mag-y','mag-z']
#mpu9250_vec,AK8963_vec,t_vec = [],[],[]
ax,ay,az,wx,wy,wz,t_vec = [],[],[],[],[],[],[]

print('recording data')
for i in range(0,N):

    a_x,a_y,a_z = accel_read() # read and convert mpu9250 data
    #mx,my,mz = AK8963_conv() # read and convert AK8963 magnetometer data

    t_vec.append(time.time()) # capture timestamp
    #AK8963_vec.append([mx,my,mz])
    ax.append(a_x)
    ay.append(a_y)
    az.append(a_z)
    
    time.sleep(max(0,dt-0.01))    

fs_actual = N/(t_vec[N-1]-t_vec[0]) #average sampling rate (approximate)
print('sample rate accel: {0:2.2f} Hz'.format(fs_actual)) # print the sample rate
t_vec = np.subtract(t_vec,t_vec[0])

# DFT
Ax = np.abs(fft(signal.detrend(ax)))
Ay = np.abs(fft(signal.detrend(ay)))
Az = np.abs(fft(signal.detrend(az)))

f = np.array(range(0,len(ax)))*fs_actual/len(ax) #frequency range

# Plotting
plt.subplot(211)
plt.plot(t_vec,ax,t_vec,ay,t_vec,az)
plt.xlabel("Time -s")
plt.ylabel("Acceleration - g")
plt.title("Accel - Time Domain")
plt.subplot(212)
plt.plot(f,Ax,f,Ay,f,Az)
plt.xlabel("Frequency - Hz")
plt.ylabel("DFT Amplitude")
plt.title("Accel - Freq Domain")
plt.show()