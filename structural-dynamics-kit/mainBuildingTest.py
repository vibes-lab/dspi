# Acquisition code for the structural dynamics building kit
# Sensors used:
#     LSM6DS3 x-axis for base motion
#     MPU6050 x-axis for roof motion

## NOTE: This requires an upgrade of the default RPi scipy library 

import lsm6ds3_i2c as acc_base
import mpu9250_i2c as acc_roof
import numpy as np
import time
from scipy import fft, signal,interpolate
import matplotlib.pyplot as plt

time.sleep(0.5) # delay necessary to allow sensor to settle

#==========================================================
# Acquisition settings
T = 10 #acquisition time period (s)
fs = 100 # sampling rate in Hz
sensor = "accel" #specify the sensor setting, other options include: "gyro" or "accel-gyro"
accel_range = '2g' #accel range +/- 2g
save_name = '/home/pi/Desktop/Test_Files/test1' #change this name save a new file
#==========================================================

#enable the FIFO buffer to read accel X,Y,Z axes
acc_base.enable_FIFO(sensor)
acc_roof.enable_FIFO(sensor)

# Set rates and filters
acc_base.setRate(fs) #set the device sampling rate, NOTE: Sampling rate options are limited, so input may not be actual
# antialias filters are set automatically by the rate selection
acc_roof.setFilter_gyro(184) #set the gyro's digital low pass filter cutoff to the nyquist frequency
acc_roof.setFilter_accel(fs/2) #set the accels's digital low pass filter cutoff to the nyquist frequency
acc_roof.setRate(fs) #set the IMU sampling rate, NOTE: Sampling rate options are whole fractions of 1000 Hz, so input may not be actual


# Display the actual values that were set
cutoff_accel_b = acc_base.getFilter_accel()
fs_actual_b = acc_base.getRate()
cutoff_accel_r = acc_roof.getFilter_accel()
fs_actual_r = acc_roof.getRate()*(1-0.015) #adding correction factor due to mismatch
print('Base Accel Sampling Rate = {0:2.2f} Hz'.format(fs_actual_b))
print('Base Accel Low Pass Cutoff = {0:2.2f} Hz'.format(cutoff_accel_b))
print('Roof Accel Sampling Rate = {0:2.2f} Hz'.format(fs_actual_r))
print('Roof Accel Low Pass Cutoff = {0:2.2f} Hz'.format(cutoff_accel_r))

# Set and get the full scale range
acc_base.setRange_accel(accel_range)
accel_range_b = acc_base.getRange_accel()
acc_base.setRange_accel(accel_range)
accel_range_r = acc_base.getRange_accel()
print('Base Accel Range = +/- {0:2.2f} g'.format(accel_range_b))
print('Roof Accel Range = +/- {0:2.2f} g'.format(accel_range_r))

# Acquisition
Nb = T*fs_actual_b #number of samples to acquire for base accel
Nr = T*fs_actual_r #number of samples to acquire for roof accel
acc_base.rst_FIFO() #reset the FIFO to clear any existing data
acc_roof.rst_FIFO() #reset the FIFO to clear any existing data

data_b = [] #empty data vector
data_r = [] #empty data vector
if sensor == "accel-gyro":
    sensor_num = 6
else:
    sensor_num = 3
ib=0 #number of time samples for base accel
ir=0 #number of time samples for roof accel

print("Starting Continuous Acquisition...")
while ib < Nb or ir < Nr:
    #This loop continuously reads the FIFO to check for new samples, it will quit if the FIFO overflows,
    #b/c that means it lost data.
    #time.sleep(.01)
    
    # Base sensor
    cnti_b = acc_base.count_FIFO() #check number of bytes in the FIFO buffer
    oflowi_b = acc_base.FIFO_overflow() #check if it overflowed since last read
    #print(cnti)
    #print(cnti)
    #print(oflowi)
    if oflowi_b == False:
        if not(cnti_b == 0) and ib < Nb:
            #if not empty
            block_b = acc_base.read_block_FIFO()
            data_b.extend(block_b)
            #data_l.extend(block_l)
            ib += len(block_b)/sensor_num/2 #for each sensor FIFO stores two bytes of data per sample
    else:
        ib = Nb+1
        print("Base Sensor FIFO Overflowed: stopping loop. Try reducing sample rate")
    
    # Base sensor
    cnti_r = acc_roof.count_FIFO() #check number of bytes in the FIFO buffer
    oflowi_r = acc_roof.FIFO_overflow() #check if it overflowed since last read
    #print(cnti)
    #print(cnti)
    #print(oflowi)
    if oflowi_r == False:
        if not(cnti_r == 0) and ir < Nr:
            #if not empty
            block_r = acc_roof.read_block_FIFO()
            data_r.extend(block_r)
            #data_l.extend(block_l)
            ir += len(block_r)/sensor_num/2 #for each sensor FIFO stores two bytes of data per sample
    else:
        ir = Nr+1
        print("Roof Sensor FIFO Overflowed: stopping loop. Try reducing sample rate")

print("Acquisition Complete.")

bx,by,bz = acc_base.conv_FIFO(data_b,sensor,Nb) #convert the digital data to physical units
rx,ry,rz = acc_roof.conv_FIFO(data_r,sensor,Nr) #convert the digital data to physical units

base_acc_0 = bx
roof_acc_0 = rx

# Filtering
order = 101
fc = np.array([0.5,fs/4]) #Hz
hb = signal.firwin(order, fc,window='blackman',pass_zero='bandpass',fs=fs_actual_b)
hr = signal.firwin(order, fc,window='blackman',pass_zero='bandpass',fs=fs_actual_r)

base_acc_f = signal.filtfilt(hb,1,signal.detrend(base_acc_0))
roof_acc_f = signal.filtfilt(hr,1,signal.detrend(roof_acc_0))


# Resample both signals to the same sampling rate
t = np.array(range(0,T*fs))/fs
base_acc = signal.resample(base_acc_f,np.size(t))
roof_acc = signal.resample(roof_acc_f,np.size(t))

# Save CSV file
saveA = np.column_stack((t,base_acc,roof_acc))
np.savetxt(save_name+'.csv',saveA,delimiter=",",header="Time (s),Base accel. (g), Roof accel. (g)")

# Plotting
print('Plotting...')
plt.plot(t,base_acc,t,roof_acc)
plt.xlabel("Time -s")
plt.ylabel("Acceleration - g")
plt.title("Building Response, dt = {0:2.2f} sec".format(1/fs))
plt.legend(['Base','Roof'])
plt.show()