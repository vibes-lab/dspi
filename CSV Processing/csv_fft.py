# CSV Plot FFT

import numpy as np
from scipy import signal
from scipy.fft import fft,fftshift
import matplotlib.pyplot as plt

# Importing
filename = '/home/pi/daqhats/examples/c/mcc172/data_logger/logger/LogFiles/data.csv'
fs = 1024
sens0 = 1000
sens1 = 10
C = np.loadtxt(filename,delimiter=',',skiprows=1,usecols=(0,1))
a0 = C[:,0]/sens0
a1 = C[:,1]/sens1
rms0 = np.round(np.sqrt(np.mean(a0**2)),decimals=4)
rms1 = np.round(np.sqrt(np.mean(a1**2)),decimals=4)

# DFT
A0 = np.abs(fft(np.multiply(np.hanning(len(a0)),signal.detrend(a0))))
A1 = np.abs(fft(np.multiply(np.hanning(len(a1)),signal.detrend(a1))))

f = np.array(range(0,len(a0)))*fs/len(a0) #frequency range

# Plotting
t = np.array(range(0,len(a0)))/fs
plt.subplot(211)
plt.plot(t,a0,t,a1)
plt.xlabel("Time -s")
plt.ylabel("Acceleration - g")
plt.legend(["Ch0, RMS = "+ str(rms0),"Ch1, RMS = "+str(rms1)])
plt.title("Accel - Time Domain")
plt.subplot(212)
plt.plot(f,A0,f,A1)
plt.xlabel("Frequency - Hz")
plt.ylabel("DFT Amplitude")
plt.title("Accel - Freq Domain")
plt.show()