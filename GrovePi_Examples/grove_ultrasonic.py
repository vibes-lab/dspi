#!/usr/bin/env python
#
# GrovePi Example for using the Grove Ultrasonic Ranger (http://www.seeedstudio.com/wiki/Grove_-_Ultrasonic_Ranger)
#
# The GrovePi connects the Raspberry Pi and Grove sensors.  You can learn more about GrovePi here:  http://www.dexterindustries.com/GrovePi
#
# Have a question about this example?  Ask on the forums here:  http://forum.dexterindustries.com/c/grovepi
#
'''
## License

The MIT License (MIT)

GrovePi for the Raspberry Pi: an open source platform for connecting Grove Sensors to the Raspberry Pi.
Copyright (C) 2017  Dexter Industries

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

import grovepi
import time
import numpy as np
import matplotlib.pyplot as plt

# set I2C to use the hardware bus
grovepi.set_bus("RPI_1")

# Connect the Grove Ultrasonic Ranger to digital port D4
# SIG,NC,VCC,GND
ultrasonic_ranger = 4

#==========================================================
# Acquisition settings
N = 10 #Number of samples 
fs = 10 # "desired" sampling rate
save_name = 'test1' #change this name save a new file
#==========================================================

value = np.zeros([0,0])
t = np.zeros([0,0])
count = 0

t0 = time.time()
while count <= N:
    try:
        # Read distance value from Ultrasonic
        i = grovepi.ultrasonicRead(ultrasonic_ranger)
        value = np.append(value,i)
        t = np.append(t,time.time()-t0)
        print(i)

    except Exception as e:
        print ("Error:{}".format(e))
    
    time.sleep(1/fs) # don't overload the i2c bus
    count = count+1

# Plotting

plt.plot(t,value)
plt.xlabel("Time -s")
plt.ylabel("Ultrasonic  - V")
plt.show()

# Save CSV file
saveD = np.column_stack((t,value))
np.savetxt(save_name+'_ultrasonic.csv',saveD,delimiter=",",header="time (s),value (V)")